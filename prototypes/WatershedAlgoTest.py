import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys

_IMG_DIMS_ = {'HFR': (2448, 2048),
              'LUM': (6576, 4384)}

newX, newY = 600,400

def load(filepath,color_type = cv2.COLOR_BAYER_BG2BGR):
    img_dims = _IMG_DIMS_['HFR']
    with open(filepath, 'rb') as filein: # do a binary read
        raw_img = np.fromfile(filein,dtype = np.uint8,
            count = img_dims[0]*img_dims[1]).reshape([x for x in img_dims[::-1]])

    return cv2.resize(cv2.cvtColor(raw_img, color_type), (newX,newY))

if __name__ == '__main__':
    filename = sys.argv[1]
    img = load(filename)
    img2 = img.copy()
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel,iterations = 2)

    sure_bg = cv2.dilate(opening,kernel,iterations=3)

    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)

    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg,sure_fg)

    while(1):
        cv2.imshow('thresh',thresh)
        cv2.imshow('grey',sure_bg)
        k = cv2.waitKey(1)
        if k == 27:
            break
    cv2.destroyAllWindows()

