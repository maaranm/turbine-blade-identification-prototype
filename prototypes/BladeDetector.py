import cv2
import numpy as np
from matplotlib import pyplot as plt
import argparse

_IMG_DIMS_ = {'HFR': (2448, 2048),
              'LUM': (6576, 4384)}

newX, newY = 600,400
clickCount = 0
global step

class BladeDetector(object):
    def __init__(self,img_dir):
        self._img_dir = img_dir
        self._img = None
        self._window = 'Viewer'
        self._clickCount = 0
        selecting = False
        refPt = []
        self._topX = -10
        self._topY = -10
        self._botX = -10
        self._botY = -10

    def load(self,filepath,color_type = cv2.COLOR_BAYER_BG2BGR):
        img_dims = _IMG_DIMS_['HFR']
        with open(filepath, 'rb') as filein: # do a binary read
            raw_img = np.fromfile(
                filein,
                dtype = np.uint8,
                count = img_dims[0]*img_dims[1]).reshape([x for x in img_dims[::-1]])

        return cv2.resize(cv2.cvtColor(raw_img, color_type), (newX,newY))

    def bladeIsolator(self,frame):
        #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        mask = np.zeros(frame.shape[:2],np.uint8)

        bgdModel = np.zeros((1,65),np.float64)
        fgdModel = np.zeros((1,65),np.float64)

        rect = (self._topX,self._topY,self._botX,self._botY)
        cv2.grabCut(frame,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)

        mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
        img = frame*mask2[:,:,np.newaxis]
        return img

    def select(self, event, x, y, flags, param):
        if step == 1:
            if event == cv2.EVENT_LBUTTONDOWN:
                selecting = True
                self._topX = x
                self._topY = y
            elif event == cv2.EVENT_LBUTTONUP:
                self._botX = x
                self._botY = y
                self._clickCount = self._clickCount + 1
            print('clicked')
    def getClick(self):
        return self._clickCount
    def getRectPoints(self):
        return self._topX,self._topY,self._botX,self._botY

    def mask(self,lower,upper,frame):
        hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        return cv2.inRange(hsv,lower,upper)


if __name__  == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--location", required=True, help="folder with image to view")
    parser.add_argument("-p", "--type", required=True, help="raw or jpeg")
    args = parser.parse_args()
    detector = BladeDetector(args.location)

    if(args.type=='jpeg'):
        frame = cv2.imread(args.location)
    else:
        frame = detector.load(args.location)
    cv2.namedWindow('original')
    cv2.setMouseCallback('original', detector.select)
    cv2.createTrackbar('l0', 'original',0,179,lambda x:None)
    cv2.createTrackbar('l1', 'original',0,179,lambda x:None)
    cv2.createTrackbar('l2', 'original',0,255,lambda x:None)
    cv2.createTrackbar('u0', 'original',0,255,lambda x:None)
    cv2.createTrackbar('u1', 'original',0,255,lambda x:None)
    cv2.createTrackbar('u2', 'original',0,255,lambda x:None)
    cv2.createTrackbar('step', 'original',0,1,lambda x:None)
    analyzed = False
    lower = np.array([0,0,0])
    upper = np.array([0,0,0])
    step = 0
    while(1):
        lower[0] = cv2.getTrackbarPos('l0','original')
        upper[0] = cv2.getTrackbarPos('u0','original')
        lower[1] = cv2.getTrackbarPos('l1','original')
        upper[1] = cv2.getTrackbarPos('u1','original')
        lower[2] = cv2.getTrackbarPos('l2','original')
        upper[2] = cv2.getTrackbarPos('u2','original')
        step = cv2.getTrackbarPos('step','original')
    # while clickCount<2:
        #cv2.imshow('original',frame)
        clickCount = detector.getClick()
        if step == 0:
            print("enter hsv values")
            cv2.imshow('original',frame)
            mask = detector.mask(lower,upper,frame)
            cv2.imshow('mask',mask)
        elif step == 1:
            print("select region")
            if clickCount > 0:
                topX, topY, botX, botY = detector.getRectPoints()
                cv2.rectangle(frame,(topX,topY),(botX,botY), (0,0,255), 3)
                cv2.imshow('original',frame)
                if analyzed == False:
                    newFrame = detector.bladeIsolator(frame)
                    analyzed == True
                cv2.imshow('edited',newFrame)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()








