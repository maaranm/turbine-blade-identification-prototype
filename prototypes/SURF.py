import cv2
import numpy as np
import os
import sys
import glob
from matplotlib import pyplot as plt


refPath = sys.argv[1]
ref = cv2.imread(refPath)

testPath = sys.argv[2]
test = cv2.imread(testPath)

MIN_COUNT = 5

sift = cv2.xfeatures2d.SIFT_create()

kp1, des1 = sift.detectAndCompute(ref, None)
kp2, des2 = sift.detectAndCompute(test, None)

FLANN_INDEX_KDTREE = 0
index = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search = dict(checks = 50)

flann = cv2.FlannBasedMatcher(index, search)
matches = flann.knnMatch(des1,des2,k=2)

good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good)>MIN_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w = ref.shape[:2]
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv2.perspectiveTransform(pts,M)

    test = cv2.polylines(test,[np.int32(dst)],True,255,3, cv2.LINE_AA)

else:
    matchesMask = None

draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)

img3 = cv2.drawMatches(ref,kp1,test,kp2,good,None,**draw_params)
cv2.imshow('img3', img3)
plt.imshow(img3, 'gray'),plt.show()

