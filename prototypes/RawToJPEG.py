import numpy as np
import cv2
import sys
import natsort
import glob
import os

_IMG_DIMS_ = {'HFR': (2448, 2048),
              'LUM': (6576, 4384)}

newX, newY = 600,400

def load(filepath,color_type = cv2.COLOR_BAYER_BG2BGR):
    img_dims = _IMG_DIMS_['HFR']
    with open(filepath, 'rb') as filein: # do a binary read
        raw_img = np.fromfile(filein,dtype = np.uint8,
            count = img_dims[0]*img_dims[1]).reshape([x for x in img_dims[::-1]])

    return cv2.resize(cv2.cvtColor(raw_img, color_type), (newX,newY))

if __name__ == '__main__':
    filepath = sys.argv[1]

    files = natsort.natsorted(glob.glob(os.path.join(filepath, '*.raw'.format('HFR'))))
    startCount = int(sys.argv[2])
    numImg = len(files)
    for x in range(startCount,startCount+numImg):
        img = load(files[x-startCount])
        cv2.imwrite(str(x)+'.jpg',img)
