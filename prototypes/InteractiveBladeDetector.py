import numpy as np
import cv2
import sys
import glob
import natsort
import os

BLUE = [255,0,0]        # rectangle color

rect = (0,0,1,1)
drawing = False
rectangle = False
rect_over = False
rect_or_mask = 100
thickness = 3

_IMG_DIMS_ = {'HFR': (2448, 2048),
              'LUM': (6576, 4384)}

newX, newY = 1000,800 #default size
#lower and upper threshold for hsv filtering
lower = np.array([0,0,0])
upper = np.array([0,0,0])

def onmouse(event,x,y,flags,param):
    """
    Mouse event callback method for setting the ROI prior to using the Grabcut Algorithm

    Only the right mouse button is used
    """
    global img,img2,drawing,value,mask,rectangle,rect,rect_or_mask,ix,iy,rect_over
    if event == cv2.EVENT_RBUTTONDOWN:
        rectangle = True
        ix,iy = x,y

    elif event == cv2.EVENT_MOUSEMOVE:
        if rectangle == True:
            img = img2.copy()
            cv2.rectangle(img,(ix,iy),(x,y),BLUE,2)
            rect = (min(ix,x),min(iy,y),abs(ix-x),abs(iy-y))
            rect_or_mask = 0

    elif event == cv2.EVENT_RBUTTONUP:
        rectangle = False
        rect_over = True
        cv2.rectangle(img,(ix,iy),(x,y),BLUE,2)
        rect = (min(ix,x),min(iy,y),abs(ix-x),abs(iy-y))
        rect_or_mask = 0

def load(filepath,color_type = cv2.COLOR_BAYER_BG2BGR): #Exact same load functionality and implementatio as VizViewer
    img_dims = _IMG_DIMS_['HFR']
    with open(filepath, 'rb') as filein: # do a binary read
        raw_img = np.fromfile(filein,dtype = np.uint8,
            count = img_dims[0]*img_dims[1]).reshape([x for x in img_dims[::-1]])
    return cv2.resize(cv2.cvtColor(raw_img, color_type), (newX,newY))

def maskFrame(lower,upper,image):
    """
    :param lower: lower threshold for masking the image
    :param upper: upper threshold for masking the image
    :param image: hsv image to be masked
    :return: masked binary image
    """
    hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    return cv2.inRange(hsv,lower,upper)

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('proper usage: python InteractiveBladeDetector.py /path/to/raw/images height width hsv mode or non hsv mode /path/to/textfile')
    else:
        filePath = sys.argv[1]
        files = natsort.natsorted(glob.glob(os.path.join(filePath, '*.raw'.format('HFR'))))
        newX = int(sys.argv[2])
        newY = int(sys.argv[3])
        mode = sys.argv[4]
        fileName = files[0]
        img = load(fileName)
        img2 = img.copy()
        gray = cv2.cvtColor(img2, cv2.COLOR_HSV2BGR)
        gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
        mask = np.zeros(img.shape[:2], dtype=np.uint8)
        output = np.zeros(img.shape, np.uint8) #blank for now
        cv2.namedWindow('output')
        cv2.namedWindow('input')
        cv2.setMouseCallback('input', onmouse)
        if (mode == 'hsv'): #keeps the clutter to a minimum by only showing sliders when hsv value needs to be adjusted
            cv2.createTrackbar('hLow', 'input', 0, 179, lambda x: None)
            cv2.createTrackbar('sLow', 'input', 0, 255, lambda x: None)
            cv2.createTrackbar('vLow', 'input', 0, 255, lambda x: None)
            cv2.createTrackbar('hHigh', 'input', 0, 179, lambda x: None)
            cv2.createTrackbar('sHigh', 'input', 0, 255, lambda x: None)
            cv2.createTrackbar('vHigh', 'input', 0, 255, lambda x: None)
            file = open(sys.argv[5], 'w')
        else:
            file = open(sys.argv[5], 'r')
            print('starting')
            for x in range(0, 3):
                lowLn = file.readline()
                highLn = file.readline()
                low = (int(lowLn))
                high = (int(highLn))
                lower[x] = low
                upper[x] = high

        maskedHSV = None
        x = 0
        loaded = True
        while (1):
            if loaded == False:
                fileName = files[x]
                img = load(fileName)
                img2 = img.copy()
                gray = cv2.cvtColor(img2, cv2.COLOR_HSV2BGR)
                gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
                loaded = True

            k = cv2.waitKey(1)
            if k == 27:
                break
            if k == ord('d'):
                x = x + 1
                loaded = False
            elif k == ord('a'):
                x = x - 1
                loaded = False
            if k == ord('n'):
                if (rect_or_mask == 0):
                    bgdmodel = np.zeros((1, 65), np.float64)
                    fgdmodel = np.zeros((1, 65), np.float64)
                    hsv = cv2.bitwise_and(img2, img2, mask=maskedHSV)
                    cv2.grabCut(hsv, mask, rect, bgdmodel, fgdmodel, 1, cv2.GC_INIT_WITH_RECT)
                    rect_or_mask = 1
                elif rect_or_mask == 1:
                    bgdmodel = np.zeros((1, 65), np.float64)
                    fgdmodel = np.zeros((1, 65), np.float64)
                    cv2.grabCut(img2, mask, rect, bgdmodel, fgdmodel, 1, cv2.GC_INIT_WITH_MASK)

                mask2 = np.where((mask == 1) + (mask == 3), 255, 0).astype('uint8')
                output = cv2.bitwise_and(img2, img2, mask=mask2)
            if mode == 'hsv':
                lower[0] = cv2.getTrackbarPos('hLow', 'input')
                upper[0] = cv2.getTrackbarPos('hHigh', 'input')
                lower[1] = cv2.getTrackbarPos('sLow', 'input')
                upper[1] = cv2.getTrackbarPos('sHigh', 'input')
                lower[2] = cv2.getTrackbarPos('vLow', 'input')
                upper[2] = cv2.getTrackbarPos('vHigh', 'input')
            maskedHSV = maskFrame(lower, upper, img2)
            cv2.imshow('test', maskedHSV)
            cv2.imshow('output', output)
            cv2.imshow('input', img)

        if mode == 'hsv':
            for x in range(0, 3):
                file.write(str(lower[x]) + '\n')
                file.write(str(upper[x]) + '\n')
        file.close()
        cv2.destroyAllWindows()
